﻿using System;

namespace ViewModel
{
    public class TheLoaiVM
    {
        public Guid IdTheLoai { set; get; }
        public string TenTheLoai { set; get; }
    }
}