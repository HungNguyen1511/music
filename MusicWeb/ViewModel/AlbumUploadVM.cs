﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace ViewModel
{
    public class AlbumUploadVM
    {
        public Guid IdAlbum { set; get; }
        public string TenAlbum { set; get; }
        public DateTime NgayPhatHanh { set; get; }
        public string AnhAlbum { set; get; }
        public Guid IdCaSi { get; set; }
        public IFormFile FormFileAnh { get; set; }
        public string BaiHat { get; set; }
    }
}
