﻿using Microsoft.AspNetCore.Http;
using System;

namespace ViewModel
{
    public class BaiHatUploadVM
    {
        public Guid IdBaiHat { set; get; }
        public string TenBaiHat { set; get; }
        public int SoLuotThich { set; get; }
        public DateTime? NgayCapNhat { set; get; }
        public string LoiBaiHat { set; get; }
        public int ThoiLuong { set; get; }
        public string FileAnh { get; set; }
        public string FileBaiHat { get; set; }
        public string IdAlbum { get; set; }
        public string IdTheLoai { get; set; }
        public IFormFile FormFileAnh { get; set; }
        public IFormFile FormFileBaiHat { get; set; }
        public string CaSi { get; set; }
    }
}