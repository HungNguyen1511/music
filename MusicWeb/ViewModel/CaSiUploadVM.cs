﻿using Microsoft.AspNetCore.Http;
using System;

namespace ViewModel
{
    public class CaSiUploadVM
    {
        public Guid IdCaSi { set; get; }
        public string TenCaSi { set; get; }
        public string TieuSu { set; get; }
        public string AnhCaSi { get; set; }
        public IFormFile FormFileAnh { get; set; }
    }
}