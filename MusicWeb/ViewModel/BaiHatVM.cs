﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace ViewModel
{
    public class BaiHatVM
    {
        public Guid IdBaiHat { set; get; }
        public string TenBaiHat { set; get; }
        public int SoLuotThich { set; get; }
        public DateTime? NgayCapNhat { set; get; }
        public string LoiBaiHat { set; get; }
        public int ThoiLuong { set; get; }
        public string FileAnh { get; set; }
        public string FileBaiHat { get; set; }
        public string IdTheLoai { get; set; }
        public string IdAlbum { get; set; }
        public List<CaSiHatBaiHatVM> CaSi { get; set; }
    }
}