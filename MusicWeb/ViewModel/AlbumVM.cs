﻿using System;
using System.Collections.Generic;

namespace ViewModel
{
    public class AlbumVM
    {
        public Guid IdAlbum { set; get; }
        public string TenAlbum { set; get; }
        public DateTime NgayPhatHanh { set; get; }
        public string AnhAlbum { set; get; }
        public CaSiVM CaSi { get; set; }
        public List<Guid> BaiHats { get; set; }
    }
}