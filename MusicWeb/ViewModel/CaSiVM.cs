﻿using System;

namespace ViewModel
{
    public class CaSiVM
    {
        public Guid IdCaSi { set; get; }
        public string TenCaSi { set; get; }
        public string TieuSu { set; get; }
        public string AnhCaSi { get; set; }
    }
}