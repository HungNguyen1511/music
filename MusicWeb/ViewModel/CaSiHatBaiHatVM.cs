﻿using System;

namespace ViewModel
{
    public class CaSiHatBaiHatVM
    {
        public Guid IdCaSi { set; get; }
        public string TenCaSi { get; set; }
    }
}