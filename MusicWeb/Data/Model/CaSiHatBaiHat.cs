﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Model
{
    public class CaSiHatBaiHat
    {
        [Key]
        public Guid Id { get; set; }

        [ForeignKey("CaSi")]
        public Guid IdCaSi { set; get; }

        public virtual CaSi CaSi { get; set; }

        [ForeignKey("BaiHat")]
        public Guid IdBaiHat { set; get; }

        public virtual BaiHat BaiHat { get; set; }
    }
}