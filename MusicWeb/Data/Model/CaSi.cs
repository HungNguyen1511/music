﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Model
{
    public class CaSi
    {
        [Key]
        public Guid IdCaSi { set; get; }

        public string TenCaSi { set; get; }
        public string TieuSu { set; get; }
        public string AnhCaSi { get; set; }

        public virtual ICollection<CaSiHatBaiHat> CaSiHatBaiHats { get; set; }
        public virtual ICollection<Album> Albums { get; set; }
    }
}