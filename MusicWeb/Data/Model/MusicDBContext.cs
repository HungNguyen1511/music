﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Data.Model
{
    public class MusicDBContext : IdentityDbContext<User>
    {
        public MusicDBContext(DbContextOptions<MusicDBContext> options) :base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=.;Initial Catalog=MusicDb;Integrated Security=True");
        }

        public DbSet<Album> Albums { get; set; }
        public DbSet<BaiHat> BaiHats { get; set; }
        public DbSet<CaSi> CaSis { get; set; }
        public DbSet<CaSiHatBaiHat> CaSiHatBaiHats { get; set; }
        public DbSet<TheLoai> TheLoais { get; set; }
    }
}