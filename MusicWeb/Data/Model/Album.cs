﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Model
{
    public class Album
    {
        [Key]
        public Guid IdAlbum { set; get; }

        public string TenAlbum { set; get; }
        public DateTime NgayPhatHanh { set; get; }
        public string AnhAlbum { set; get; }

        [ForeignKey("CaSi")]
        public Guid IdCaSi { set; get; }
        public virtual List<BaiHat> BaiHats { get; set; }
        public virtual CaSi CaSi { get; set; }
    }
}