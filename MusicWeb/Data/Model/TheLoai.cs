﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Model
{
    public class TheLoai
    {
        [Key]
        public Guid IdTheLoai { set; get; }

        public string TenTheLoai { set; get; }
        public virtual ICollection<BaiHat> BaiHats { get; set; }
    }
}