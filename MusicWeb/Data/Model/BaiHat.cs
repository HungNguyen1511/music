﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Model
{
    public class BaiHat
    {
        [Key]
        public Guid IdBaiHat { set; get; }

        public string TenBaiHat { set; get; }
        public int SoLuotThich { set; get; }
        public DateTime NgayCapNhat { set; get; }
        public string LoiBaiHat { set; get; }
        public int ThoiLuong { set; get; }

        [ForeignKey("Album")]
        public Guid? IdAlbum { set; get; }

        public virtual Album Album { get; set; }

        [ForeignKey("TheLoai")]
        public Guid IdTheLoai { set; get; }
        public virtual TheLoai TheLoai { get; set; }
        public virtual ICollection<CaSiHatBaiHat> CaSiHatBaiHats { get; set; }
        public int SoLuotNghe { get; set; }
        public string FileAnh { get; set; }
        public string FileBaiHat { get; set; }
    }
}