﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Data.Migrations
{
    public partial class updatemodelid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CaSis",
                columns: table => new
                {
                    IdCaSi = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TenCaSi = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TieuSu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AnhCaSi = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaSis", x => x.IdCaSi);
                });

            migrationBuilder.CreateTable(
                name: "TheLoais",
                columns: table => new
                {
                    IdTheLoai = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TenTheLoai = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TheLoais", x => x.IdTheLoai);
                });

            migrationBuilder.CreateTable(
                name: "Albums",
                columns: table => new
                {
                    IdAlbum = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TenAlbum = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NgayPhatHanh = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AnhAlbum = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IdCaSi = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Albums", x => x.IdAlbum);
                    table.ForeignKey(
                        name: "FK_Albums_CaSis_IdCaSi",
                        column: x => x.IdCaSi,
                        principalTable: "CaSis",
                        principalColumn: "IdCaSi",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BaiHats",
                columns: table => new
                {
                    IdBaiHat = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TenBaiHat = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SoLuotThich = table.Column<int>(type: "int", nullable: false),
                    NgayCapNhat = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LoiBaiHat = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ThoiLuong = table.Column<int>(type: "int", nullable: false),
                    IdAlbum = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    IdTheLoai = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaiHats", x => x.IdBaiHat);
                    table.ForeignKey(
                        name: "FK_BaiHats_Albums_IdAlbum",
                        column: x => x.IdAlbum,
                        principalTable: "Albums",
                        principalColumn: "IdAlbum",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaiHats_TheLoais_IdTheLoai",
                        column: x => x.IdTheLoai,
                        principalTable: "TheLoais",
                        principalColumn: "IdTheLoai",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CaSiHatBaiHats",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdCaSi = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdBaiHat = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaSiHatBaiHats", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CaSiHatBaiHats_BaiHats_IdBaiHat",
                        column: x => x.IdBaiHat,
                        principalTable: "BaiHats",
                        principalColumn: "IdBaiHat",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CaSiHatBaiHats_CaSis_IdCaSi",
                        column: x => x.IdCaSi,
                        principalTable: "CaSis",
                        principalColumn: "IdCaSi",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Albums_IdCaSi",
                table: "Albums",
                column: "IdCaSi");

            migrationBuilder.CreateIndex(
                name: "IX_BaiHats_IdAlbum",
                table: "BaiHats",
                column: "IdAlbum");

            migrationBuilder.CreateIndex(
                name: "IX_BaiHats_IdTheLoai",
                table: "BaiHats",
                column: "IdTheLoai");

            migrationBuilder.CreateIndex(
                name: "IX_CaSiHatBaiHats_IdBaiHat",
                table: "CaSiHatBaiHats",
                column: "IdBaiHat");

            migrationBuilder.CreateIndex(
                name: "IX_CaSiHatBaiHats_IdCaSi",
                table: "CaSiHatBaiHats",
                column: "IdCaSi");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CaSiHatBaiHats");

            migrationBuilder.DropTable(
                name: "BaiHats");

            migrationBuilder.DropTable(
                name: "Albums");

            migrationBuilder.DropTable(
                name: "TheLoais");

            migrationBuilder.DropTable(
                name: "CaSis");
        }
    }
}