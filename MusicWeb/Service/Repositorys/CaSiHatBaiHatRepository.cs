﻿using Data.Model;
using Microsoft.EntityFrameworkCore;
using Service.Repositorys.IRepository;
using Service.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Service.Repositorys
{
    public class CaSiHatBaiHatRepository : BaseRepository<CaSiHatBaiHat>, ICaSiHatBaiHatRepository
    {
        private readonly MusicDBContext _dBContext;
        public CaSiHatBaiHatRepository(MusicDBContext dbContext) : base(dbContext)
        {
            _dBContext = dbContext;
        }

        public List<CaSiHatBaiHat> GetByBaiHat(Guid id)
        {
            return _dBContext.CaSiHatBaiHats.Include(x => x.CaSi).Where(x => x.IdBaiHat == id).ToList();
        }
    }
}