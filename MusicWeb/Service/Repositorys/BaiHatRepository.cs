﻿using Data.Model;
using Microsoft.EntityFrameworkCore;
using Service.Repositorys.IRepository;
using Service.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Repositorys
{
    public class BaiHatRepository : BaseRepository<BaiHat>, IBaiHatRepository
    {
        private readonly MusicDBContext _dbContext;
        public BaiHatRepository(MusicDBContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public List<BaiHat> GetBaiHatTheoAlbum(string idAlbum)
        {
            Guid id = Guid.Parse(idAlbum);
            var baiHats = _dbContext.BaiHats.Where(x => x.IdAlbum == id).OrderBy(x => x.TenBaiHat).ToList();
            return baiHats;
        }

        public List<BaiHat> GetBaiHatTheoCaSi(string idCaSi)
        {
            Guid id = Guid.Parse(idCaSi);
            var query = from casi in _dbContext.CaSis
                        join baihattheocasi in _dbContext.CaSiHatBaiHats on casi.IdCaSi equals baihattheocasi.IdCaSi
                        join baihat in _dbContext.BaiHats on baihattheocasi.IdBaiHat equals baihat.IdBaiHat
                        where casi.IdCaSi == id
                        select new BaiHat{
                            IdBaiHat = baihat.IdBaiHat,
                            TenBaiHat = baihat.TenBaiHat,
                            LoiBaiHat = baihat.LoiBaiHat,
                            NgayCapNhat = baihat.NgayCapNhat,
                            SoLuotThich = baihat.SoLuotThich,
                            ThoiLuong = baihat.ThoiLuong,
                            FileAnh = baihat.FileAnh,
                            FileBaiHat = baihat.FileBaiHat,
                            IdTheLoai = baihat.IdTheLoai,
                            IdAlbum = baihat.IdAlbum,
                            SoLuotNghe = baihat.SoLuotNghe,
                            Album = baihat.Album,
                            TheLoai = baihat.TheLoai,
                            CaSiHatBaiHats = baihat.CaSiHatBaiHats
                        };
            return query.ToList();
        }

        public List<BaiHat> GetBaiHatTheoTheLoai(string idTheLoai)
        {
            Guid id = Guid.Parse(idTheLoai);
            var baiHats = _dbContext.BaiHats.Where(x => x.IdTheLoai == id).OrderByDescending(x => x.NgayCapNhat).ToList();
            return baiHats;
        }

        public List<BaiHat> SearchBaiHat(string querry)
        {
            var baiHats = _dbContext.BaiHats.Where(x => x.TenBaiHat.Contains(querry) || x.LoiBaiHat.Contains(querry)).ToList();
            return baiHats;
        }

        public List<BaiHat> NewestBaiHat(int number)
        {
            var baiHats = _dbContext.BaiHats.OrderByDescending(x => x.NgayCapNhat).Take(number).ToList();
            return baiHats;
        }
    }
}