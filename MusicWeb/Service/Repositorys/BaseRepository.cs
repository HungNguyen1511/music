﻿using Data.Model;
using Microsoft.EntityFrameworkCore;
using Service.Repositorys.IRepository;
using Service.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Service.Repositorys
{
    public class BaseRepository<T> : IRepositoryBase<T> where T : class
    {
        private readonly MusicDBContext _dbContext;
        private readonly DbSet<T> dbSet;

        protected BaseRepository(MusicDBContext dbContext)
        {
            _dbContext = dbContext;
            dbSet = _dbContext.Set<T>();
        }

        public void Add(T entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(T entity)
        {
            dbSet.Remove(entity);
        }

        public virtual void Delete(Guid id)
        {
            var entity = dbSet.Find(id);
            dbSet.Remove(entity);
        }

        public ICollection<T> GetAll()
        {
            return _dbContext.Set<T>().ToList();
        }

        public T GetById(Guid id)
        {          
            T entity = _dbContext.Set<T>().Find(id);
            if (entity == null) return null;
            _dbContext.Entry(entity).State = EntityState.Detached;
            return entity;
        }

        public void Update(T entity)
        {
            dbSet.Update(entity);
        }
    }
}