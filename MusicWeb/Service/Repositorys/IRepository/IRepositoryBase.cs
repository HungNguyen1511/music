﻿using System;
using System.Collections.Generic;

namespace Service.Repositorys.IRepository
{
    public interface IRepositoryBase<T>
    {
        T GetById(Guid id);

        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);

        void Delete(Guid id);

        ICollection<T> GetAll();
    }
}