﻿using Data.Model;
using System.Collections.Generic;

namespace Service.Repositorys.IRepository
{
    public interface ICaSiRepository : IRepositoryBase<CaSi>
    {
        List<CaSi> SearchCaSi(string querry);
    }
}