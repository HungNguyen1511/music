﻿using Data.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Repositorys.IRepository
{
    public interface IAlbumRepository : IRepositoryBase<Album>
    {
        List<Album> GetNewestAlbum(int number);
        List<Album> GetAlbumCasi(string idCaSi);
        List<Album> SearchAlbum(string search);
    }
}