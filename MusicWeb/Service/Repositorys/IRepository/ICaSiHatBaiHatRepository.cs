﻿using Data.Model;
using System;
using System.Collections.Generic;

namespace Service.Repositorys.IRepository
{
    public interface ICaSiHatBaiHatRepository : IRepositoryBase<CaSiHatBaiHat>
    {
        List<CaSiHatBaiHat> GetByBaiHat(Guid id);
    }
}