﻿using Data.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Repositorys.IRepository
{
    public interface IBaiHatRepository : IRepositoryBase<BaiHat>
    {
        List<BaiHat> GetBaiHatTheoAlbum(string idAlbum);
        List<BaiHat> GetBaiHatTheoCaSi(string idCaSi);
        List<BaiHat> GetBaiHatTheoTheLoai(string idTheLoai);
        List<BaiHat> SearchBaiHat(string querry);
        List<BaiHat> NewestBaiHat(int number);
    }
}