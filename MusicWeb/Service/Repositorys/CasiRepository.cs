﻿using Data.Model;
using Microsoft.EntityFrameworkCore;
using Service.Repositorys.IRepository;
using Service.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Repositorys
{
    public class CaSiRepository : BaseRepository<CaSi>, ICaSiRepository
    {
        private readonly MusicDBContext _dbContext;
        public CaSiRepository(MusicDBContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public List<CaSi> SearchCaSi(string querry)
        {
            var casi = _dbContext.CaSis.Where(x => x.TenCaSi.Contains(querry)).ToList();
            return casi;
        }
    }
}