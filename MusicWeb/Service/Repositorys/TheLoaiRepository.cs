﻿using Data.Model;
using Service.Repositorys.IRepository;
using Service.UnitOfWork;

namespace Service.Repositorys
{
    public class TheLoaiRepository : BaseRepository<TheLoai>, ITheLoaiRepository
    {
        public TheLoaiRepository(MusicDBContext dbContext) : base(dbContext)
        {
        }
    }
}