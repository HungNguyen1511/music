﻿using Data.Model;
using Microsoft.EntityFrameworkCore;
using Service.Repositorys.IRepository;
using Service.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Repositorys
{
    public class AlbumRepository : BaseRepository<Album>, IAlbumRepository
    {
        private readonly MusicDBContext _dbContext;
        public AlbumRepository(MusicDBContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public List<Album> GetNewestAlbum(int number)
        {
            var albums = _dbContext.Albums.OrderByDescending(x => x.NgayPhatHanh).Take(number).ToList();
            return albums;
        }

        public List<Album> GetAlbumCasi(string idCaSi)
        {
            Guid id = Guid.Parse(idCaSi);
            var albums = _dbContext.Albums.Where(x => x.CaSi.IdCaSi == id).ToList();
            return albums;
        }
        public List<Album> SearchAlbum(string search)
        {
            var albums = _dbContext.Albums.Where(x => x.TenAlbum.Contains(search)).ToList();
            return albums;
        }
    }
}