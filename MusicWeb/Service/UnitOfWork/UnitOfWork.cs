using Data.Model;

namespace Service.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MusicDBContext _dbContext;

        public UnitOfWork(MusicDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool Commit()
        {
            return _dbContext.SaveChanges() > 0;
        }
        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}