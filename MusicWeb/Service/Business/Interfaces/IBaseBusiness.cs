﻿using System;
using System.Collections.Generic;

namespace Service.Business.Interfaces
{
    public interface IBaseBusiness<T>
    {
        List<T> GetAll();

        bool Add(T entity);

        bool Update(T entity);

        bool Delete(Guid id);

        T Detail(Guid id);
    }
}