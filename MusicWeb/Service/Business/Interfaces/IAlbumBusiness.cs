﻿using System.Collections.Generic;
using ViewModel;

namespace Service.Business.Interfaces
{
    public interface IAlbumBusiness: IBaseBusiness<AlbumVM>
    {
        List<AlbumVM> GetNewestAlbum(int number);
        List<AlbumVM> GetAlbumCasi(string idCaSi);
        List<AlbumVM> SearchAlbum(string search);
    }
}
