﻿using System;
using System.Collections.Generic;
using ViewModel;

namespace Service.Business.Interfaces
{
    public interface ICaSiHatBaiHatBusiness : IBaseBusiness<CaSiHatBaiHatVM>
    {
        bool Add(Guid idBaiHat, Guid idCaSi);
        bool Delete(Guid idBaiHat, Guid idCaSi);
        List<CaSiHatBaiHatVM> GetByBaiHat(Guid id);
    }
}
