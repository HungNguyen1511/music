﻿using System;
using System.Collections.Generic;
using System.Text;
using ViewModel;

namespace Service.Business.Interfaces
{
    public interface IBaiHatBusiness : IBaseBusiness<BaiHatVM>
    {
        List<BaiHatVM> GetBaiHatTheoAlbum(string idAlbum);
        List<BaiHatVM> GetBaiHatTheoCaSi(string idCaSi);
        List<BaiHatVM> GetBaiHatTheoTheLoai(string idTheLoai);
        List<BaiHatVM> SearchBaiHat(string querry);
        List<BaiHatVM> NewestBaiHat(int number);
    }
}
