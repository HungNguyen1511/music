﻿using System;
using System.Collections.Generic;
using System.Text;
using ViewModel;

namespace Service.Business.Interfaces
{
    public interface ICaSiBusiness: IBaseBusiness<CaSiVM>
    {
        List<CaSiVM> SearchCaSi(string querry);
        List<CaSiVM> GetByMusic(Guid id);
    }
}
