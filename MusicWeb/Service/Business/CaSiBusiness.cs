﻿using Data.Model;
using Service.Business.Interfaces;
using Service.Repositorys.IRepository;
using Service.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using ViewModel;

namespace Service.Business
{
    public class CaSiBusiness : ICaSiBusiness
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICaSiRepository _caSiRepository;
        private readonly ICaSiHatBaiHatRepository _caSiHatBaiHatRepository;

        public CaSiBusiness(IUnitOfWork unitOfWork, ICaSiRepository caSiRepository, ICaSiHatBaiHatRepository caSiHatBaiHatRepository)
        {
            _unitOfWork = unitOfWork;
            _caSiRepository = caSiRepository;
            _caSiHatBaiHatRepository = caSiHatBaiHatRepository;
        }

        public bool Add(CaSiVM caSiVM)
        {
            var caSi = new CaSi()
            {
                IdCaSi = caSiVM.IdCaSi,
                TenCaSi = caSiVM.TenCaSi,
                AnhCaSi = caSiVM.AnhCaSi,
                TieuSu = caSiVM.TieuSu
            };
            _caSiRepository.Add(caSi);
            return _unitOfWork.Commit();
        }

        public bool Update(CaSiVM caSiVM)
        {
            var caSi = new CaSi()
            {
                IdCaSi = caSiVM.IdCaSi,
                TenCaSi = caSiVM.TenCaSi,
                AnhCaSi = caSiVM.AnhCaSi,
                TieuSu = caSiVM.TieuSu
            };
            _caSiRepository.Update(caSi);
            return _unitOfWork.Commit();
        }

        public bool Delete(Guid id)
        {
            _caSiRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public CaSiVM Detail(Guid id)
        {
            var caSi = _caSiRepository.GetById(id);
            if (caSi == null) return null;
            var caSiVM = new CaSiVM()
            {
                IdCaSi = caSi.IdCaSi,
                TenCaSi = caSi.TenCaSi,
                AnhCaSi = caSi.AnhCaSi,
                TieuSu = caSi.TieuSu
            };
            return caSiVM;
        }

        public List<CaSiVM> GetAll()
        {
            var caSiList = _caSiRepository.GetAll().OrderBy(x => x.TenCaSi);
            var caSiVMList = new List<CaSiVM>();
            foreach (var item in caSiList)
            {
                caSiVMList.Add(new CaSiVM()
                {
                    IdCaSi = item.IdCaSi,
                    TenCaSi = item.TenCaSi,
                    AnhCaSi = item.AnhCaSi,
                    TieuSu = item.TieuSu
                });
            }
            return caSiVMList;
        }

        public List<CaSiVM> SearchCaSi(string querry)
        {
            var caSiList = _caSiRepository.SearchCaSi(querry);
            var caSiVMList = new List<CaSiVM>();
            foreach (var item in caSiList)
            {
                caSiVMList.Add(new CaSiVM()
                {
                    IdCaSi = item.IdCaSi,
                    TenCaSi = item.TenCaSi,
                    AnhCaSi = item.AnhCaSi,
                    TieuSu = item.TieuSu
                });
            }
            return caSiVMList;
        }

        public List<CaSiVM> GetByMusic(Guid id)
        {
            var caSiList = _caSiHatBaiHatRepository.GetByBaiHat(id);
            var caSiVMList = new List<CaSiVM>();
            foreach (var item in caSiList)
            {
                var caSi = _caSiRepository.GetById(item.IdCaSi);
                caSiVMList.Add(new CaSiVM()
                {
                    IdCaSi = caSi.IdCaSi,
                    TenCaSi = caSi.TenCaSi,
                    AnhCaSi = caSi.AnhCaSi,
                    TieuSu = caSi.TieuSu
                });
            }
            return caSiVMList;
        }
    }
}