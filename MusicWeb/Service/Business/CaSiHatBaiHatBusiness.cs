﻿using Data.Model;
using Service.Business.Interfaces;
using Service.Repositorys.IRepository;
using Service.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViewModel;

namespace Service.Business
{
    public class CaSiHatBaiHatBusiness : ICaSiHatBaiHatBusiness
    {
        private readonly ICaSiHatBaiHatRepository _caSiHatBaiHatRepository;
        private readonly ICaSiRepository _caSiRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CaSiHatBaiHatBusiness(ICaSiHatBaiHatRepository baiHatRepository, ICaSiRepository caSiRepository, IUnitOfWork unitOfWork)
        {
            _caSiHatBaiHatRepository = baiHatRepository;
            _caSiRepository = caSiRepository;
            _unitOfWork = unitOfWork;
        }
        public bool Add(CaSiHatBaiHatVM entity)
        {
            //var chVM = new CaSiHatBaiHat()
            //{
            //    Id = Guid.NewGuid(),
            //    IdCaSi = entity.IdCaSi
            //};
            //_caSiHatBaiHatRepository.Add(chVM);
            //return _unitOfWork.Commit();
            return true;
        }

        public bool Add(Guid idBaiHat, Guid idCaSi)
        {
            var chVM = new CaSiHatBaiHat()
            {
                Id = Guid.NewGuid(),
                IdBaiHat = idBaiHat,
                IdCaSi = idCaSi
            };
            _caSiHatBaiHatRepository.Add(chVM);
            return _unitOfWork.Commit();
        }

        public bool Delete(Guid idBaiHat, Guid idCaSi)
        {
            var entity = _caSiHatBaiHatRepository.GetAll()
                .Where(x => x.IdBaiHat == idBaiHat && x.IdCaSi == idCaSi)
                .FirstOrDefault();
            _caSiHatBaiHatRepository.Delete(entity);
            return _unitOfWork.Commit();
        }

        public bool Delete(Guid id)
        {
            //_caSiHatBaiHatRepository.Delete(id);
            //return _unitOfWork.Commit();
            return true;
        }

        public CaSiHatBaiHatVM Detail(Guid id)
        {
            var entity = _caSiHatBaiHatRepository.GetById(id);
            var casi = _caSiRepository.GetById(entity.IdCaSi);
            var chVM = new CaSiHatBaiHatVM()
            {
                IdCaSi = entity.IdCaSi,
                TenCaSi = casi.TenCaSi  
            };
            return chVM;
        }

        public List<CaSiHatBaiHatVM> GetAll()
        {
            var list = _caSiHatBaiHatRepository.GetAll();
            var chVMList = new List<CaSiHatBaiHatVM>();
            foreach (var entity in list)
            {
                var casi = _caSiRepository.GetById(entity.IdCaSi);
                chVMList.Add(new CaSiHatBaiHatVM()
                {
                    IdCaSi = entity.IdCaSi,
                    TenCaSi = casi.TenCaSi
                });
            }
            return chVMList;
        }

        public List<CaSiHatBaiHatVM> GetByBaiHat(Guid id)
        {
            var list = _caSiHatBaiHatRepository.GetByBaiHat(id);
            var chVMList = new List<CaSiHatBaiHatVM>();
            foreach (var entity in list)
            {
                var casi = _caSiRepository.GetById(entity.IdCaSi);
                chVMList.Add(new CaSiHatBaiHatVM()
                {
                    IdCaSi = entity.IdCaSi,
                    TenCaSi = casi.TenCaSi
                });
            }
            return chVMList;
        }

        public bool Update(CaSiHatBaiHatVM entity)
        {
            //var chVM = new CaSiHatBaiHat()
            //{
            //    Id = Guid.NewGuid(),
            //    IdCaSi = entity.IdCaSi
            //};
            //_caSiHatBaiHatRepository.Update(chVM);
            //return _unitOfWork.Commit();
            return true;
        }


    }
}
