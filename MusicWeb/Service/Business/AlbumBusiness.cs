﻿using Data.Model;
using Service.Business.Interfaces;
using Service.Repositorys.IRepository;
using Service.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;

namespace Service.Business
{
    public class AlbumBusiness: IAlbumBusiness
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAlbumRepository _albumRepository;
        private readonly ICaSiRepository _caSiRepository;
        private readonly IBaiHatRepository _baiHatRepository;

        public AlbumBusiness(IUnitOfWork unitOfWork, IAlbumRepository albumRepository, ICaSiRepository caSiRepository,IBaiHatRepository baiHatRepository)
        {
            _unitOfWork = unitOfWork;
            _albumRepository = albumRepository;
            _caSiRepository = caSiRepository;
            _baiHatRepository = baiHatRepository;
        }

        public bool Add(AlbumVM albumVM)
        {
            
            var album = new Album()
            {
                IdAlbum = albumVM.IdAlbum,
                TenAlbum = albumVM.TenAlbum,
                AnhAlbum = albumVM.AnhAlbum,
                NgayPhatHanh = albumVM.NgayPhatHanh,
                IdCaSi = albumVM.CaSi.IdCaSi
            };
            _albumRepository.Add(album);

            foreach (var baiHat in albumVM.BaiHats)
            {
                var newBaiHat = _baiHatRepository.GetById(baiHat);
                newBaiHat.IdAlbum = albumVM.IdAlbum;
                _baiHatRepository.Update(newBaiHat);
            }

            return _unitOfWork.Commit();
        }

        public bool Update(AlbumVM albumVM)
        {
            var baiHats = _baiHatRepository.GetBaiHatTheoAlbum(albumVM.IdAlbum.ToString());
            foreach (var baiHat in baiHats)
            {
                var newBaiHat = _baiHatRepository.GetById(baiHat.IdBaiHat);
                newBaiHat.IdAlbum = null;
                _baiHatRepository.Update(newBaiHat);
            }

            var album = new Album()
            {
                IdAlbum = albumVM.IdAlbum,
                TenAlbum = albumVM.TenAlbum,
                AnhAlbum = albumVM.AnhAlbum,
                NgayPhatHanh = albumVM.NgayPhatHanh,
                IdCaSi = albumVM.CaSi.IdCaSi,
                BaiHats = new List<BaiHat>()
            };
            _albumRepository.Update(album);
            

            foreach (var baiHat in albumVM.BaiHats)
            {
                var newBaiHat = _baiHatRepository.GetById(baiHat);
                newBaiHat.IdAlbum = albumVM.IdAlbum;
                _baiHatRepository.Update(newBaiHat);
            }

            return _unitOfWork.Commit();
        }

        public bool Delete(Guid id)
        {
            _albumRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public AlbumVM Detail(Guid id)
        {
            var album = _albumRepository.GetById(id);
            if (album == null) return null;
            var caSi = _caSiRepository.GetById(album.IdCaSi);
            var caSiVM = new CaSiVM()
            {
                IdCaSi = caSi.IdCaSi,
                TenCaSi = caSi.TenCaSi,
                AnhCaSi = caSi.AnhCaSi,
                TieuSu = caSi.TieuSu
            };

            var idBaiHats = new List<Guid>();
            var baiHats = _baiHatRepository.GetBaiHatTheoAlbum(album.IdAlbum.ToString());
            if (baiHats != null)
            {
                foreach (var baiHat in baiHats)
                {
                    idBaiHats.Add(baiHat.IdBaiHat);
                }
            }

            var albumVM = new AlbumVM()
            {
                IdAlbum = album.IdAlbum,
                TenAlbum = album.TenAlbum,
                AnhAlbum = album.AnhAlbum,
                NgayPhatHanh = album.NgayPhatHanh,
                CaSi = caSiVM,
                BaiHats = idBaiHats
            };
            return albumVM;
        }

        public List<AlbumVM> GetAll()
        {
            var albums = _albumRepository.GetAll();
            var albumVMList = new List<AlbumVM>();
            foreach (var album in albums)
            {
                var caSi = _caSiRepository.GetById(album.IdCaSi);
                var caSiVM = new CaSiVM()
                {
                    IdCaSi = caSi.IdCaSi,
                    TenCaSi = caSi.TenCaSi,
                    AnhCaSi = caSi.AnhCaSi,
                    TieuSu = caSi.TieuSu
                };

                var idBaiHats = new List<Guid>();
                var baiHats = _baiHatRepository.GetBaiHatTheoAlbum(album.IdAlbum.ToString());
                if (baiHats != null)
                {
                    foreach (var baiHat in baiHats)
                    {
                        idBaiHats.Add(baiHat.IdBaiHat);
                    }
                }

                albumVMList.Add(new AlbumVM()
                {
                   IdAlbum = album.IdAlbum,
                   AnhAlbum = album.AnhAlbum,
                   NgayPhatHanh = album.NgayPhatHanh,
                   TenAlbum = album.TenAlbum,
                   CaSi = caSiVM,
                   BaiHats = idBaiHats
                });
            }
            return albumVMList;
        }

        public List<AlbumVM> GetNewestAlbum(int number)
        {
            var albums = _albumRepository.GetNewestAlbum(number);
            var albumVMList = new List<AlbumVM>();
            foreach (var album in albums)
            {
                var caSi = _caSiRepository.GetById(album.IdCaSi);
                var caSiVM = new CaSiVM()
                {
                    IdCaSi = caSi.IdCaSi,
                    TenCaSi = caSi.TenCaSi,
                    AnhCaSi = caSi.AnhCaSi,
                    TieuSu = caSi.TieuSu
                };

                var idBaiHats = new List<Guid>();
                var baiHats = _baiHatRepository.GetBaiHatTheoAlbum(album.IdAlbum.ToString());
                if (baiHats != null)
                {
                    foreach (var baiHat in baiHats)
                    {
                        idBaiHats.Add(baiHat.IdBaiHat);
                    }
                }

                albumVMList.Add(new AlbumVM()
                {
                    IdAlbum = album.IdAlbum,
                    AnhAlbum = album.AnhAlbum,
                    NgayPhatHanh = album.NgayPhatHanh,
                    TenAlbum = album.TenAlbum,
                    CaSi = caSiVM,
                    BaiHats = idBaiHats
                });
            }
            return albumVMList;
        }

        public List<AlbumVM> GetAlbumCasi(string idCaSi)
        {
            var albums = _albumRepository.GetAlbumCasi(idCaSi);
            var albumVMList = new List<AlbumVM>();
            foreach (var album in albums)
            {
                var caSi = _caSiRepository.GetById(album.IdCaSi);
                var caSiVM = new CaSiVM()
                {
                    IdCaSi = caSi.IdCaSi,
                    TenCaSi = caSi.TenCaSi,
                    AnhCaSi = caSi.AnhCaSi,
                    TieuSu = caSi.TieuSu
                };

                var idBaiHats = new List<Guid>();
                var baiHats = _baiHatRepository.GetBaiHatTheoAlbum(album.IdAlbum.ToString());
                if (baiHats != null)
                {
                    foreach (var baiHat in baiHats)
                    {
                        idBaiHats.Add(baiHat.IdBaiHat);
                    }
                }

                albumVMList.Add(new AlbumVM()
                {
                    IdAlbum = album.IdAlbum,
                    AnhAlbum = album.AnhAlbum,
                    NgayPhatHanh = album.NgayPhatHanh,
                    TenAlbum = album.TenAlbum,
                    CaSi = caSiVM,
                    BaiHats = idBaiHats
                });
            }
            return albumVMList;
        }

        public List<AlbumVM> SearchAlbum(string search)
        {
            var albums = _albumRepository.SearchAlbum(search);
            var albumVMList = new List<AlbumVM>();
            foreach (var album in albums)
            {
                var caSi = _caSiRepository.GetById(album.IdCaSi);
                var caSiVM = new CaSiVM()
                {
                    IdCaSi = caSi.IdCaSi,
                    TenCaSi = caSi.TenCaSi,
                    AnhCaSi = caSi.AnhCaSi,
                    TieuSu = caSi.TieuSu
                };

                var idBaiHats = new List<Guid>();
                var baiHats = _baiHatRepository.GetBaiHatTheoAlbum(album.IdAlbum.ToString());
                if (baiHats != null)
                {
                    foreach (var baiHat in baiHats)
                    {
                        idBaiHats.Add(baiHat.IdBaiHat);
                    }
                }

                albumVMList.Add(new AlbumVM()
                {
                    IdAlbum = album.IdAlbum,
                    AnhAlbum = album.AnhAlbum,
                    NgayPhatHanh = album.NgayPhatHanh,
                    TenAlbum = album.TenAlbum,
                    CaSi = caSiVM,
                    BaiHats = idBaiHats
                });
            }
            return albumVMList;
        }
    }
}