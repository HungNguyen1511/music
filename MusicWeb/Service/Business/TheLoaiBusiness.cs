﻿using Data.Model;
using Service.Business.Interfaces;
using Service.Repositorys.IRepository;
using Service.UnitOfWork;
using System;
using System.Collections.Generic;
using ViewModel;

namespace Service.Business
{
    public class TheLoaiBusiness : ITheLoaiBusiness
    {
        private readonly ITheLoaiRepository _theloaiRepository;
        private readonly IUnitOfWork _unitOfWork;

        public TheLoaiBusiness(ITheLoaiRepository theLoaiRepository, IUnitOfWork unitOfWork)
        {
            _theloaiRepository = theLoaiRepository;
            _unitOfWork = unitOfWork;
        }

        public bool Add(TheLoaiVM theLoaiVM)
        {
            var theLoai = new TheLoai()
            {
                IdTheLoai = theLoaiVM.IdTheLoai,
                TenTheLoai = theLoaiVM.TenTheLoai
            };
            _theloaiRepository.Add(theLoai);
            return _unitOfWork.Commit();
        }

        public bool Update(TheLoaiVM theLoaiVM)
        {
            var theLoai = new TheLoai()
            {
                IdTheLoai = theLoaiVM.IdTheLoai,
                TenTheLoai = theLoaiVM.TenTheLoai
            };
            _theloaiRepository.Update(theLoai);
            return _unitOfWork.Commit();
        }

        public bool Delete(Guid id)
        {
            _theloaiRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public TheLoaiVM Detail(Guid id)
        {
            var theLoai = _theloaiRepository.GetById(id);
            if (theLoai == null) return null;
            var theLoaiVM = new TheLoaiVM()
            {
                IdTheLoai = theLoai.IdTheLoai,
                TenTheLoai = theLoai.TenTheLoai
            };
            return theLoaiVM;
        }

        public List<TheLoaiVM> GetAll()
        {
            var theLoaiList = _theloaiRepository.GetAll();
            var theLoaiVMList = new List<TheLoaiVM>();
            foreach (var item in theLoaiList)
            {
                theLoaiVMList.Add(new TheLoaiVM()
                {
                    IdTheLoai = item.IdTheLoai,
                    TenTheLoai = item.TenTheLoai
                });
            }
            return theLoaiVMList;
        }
    }
}