﻿using Data.Model;
using Service.Business.Interfaces;
using Service.Repositorys.IRepository;
using Service.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using ViewModel;

namespace Service.Business
{
    public class BaiHatBusiness:IBaiHatBusiness
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBaiHatRepository _baiHatRepository;
        private readonly ICaSiHatBaiHatRepository _caSiHatBaiHatRepository;
        private readonly ICaSiRepository _caSiRepository;

        public BaiHatBusiness(IUnitOfWork unitOfWork, IBaiHatRepository baiHatRepository, ICaSiHatBaiHatRepository caSiHatBaiHatRepository, ICaSiRepository caSiRepository)
        {
            _unitOfWork = unitOfWork;
            _baiHatRepository = baiHatRepository;
            _caSiHatBaiHatRepository = caSiHatBaiHatRepository;
            _caSiRepository = caSiRepository;
        }

        public bool Add(BaiHatVM baiHatVM)
        {
            var baiHat = new BaiHat()
            {
                IdBaiHat = baiHatVM.IdBaiHat,
                TenBaiHat = baiHatVM.TenBaiHat,
                LoiBaiHat = baiHatVM.LoiBaiHat,
                NgayCapNhat = baiHatVM.NgayCapNhat.Value,
                SoLuotThich = baiHatVM.SoLuotThich,
                ThoiLuong = baiHatVM.ThoiLuong,
                FileAnh = baiHatVM.FileAnh,
                FileBaiHat = baiHatVM.FileBaiHat,
                IdTheLoai = Guid.Parse(baiHatVM.IdTheLoai),                   
            };

            if (!String.IsNullOrEmpty(baiHatVM.IdAlbum))
            {
                baiHat.IdAlbum = Guid.Parse(baiHatVM.IdAlbum);
            }

            _baiHatRepository.Add(baiHat);

            foreach (var caSi in baiHatVM.CaSi)
            {
                var chVM = new CaSiHatBaiHat()
                {
                    Id = Guid.NewGuid(),
                    IdBaiHat = baiHatVM.IdBaiHat,
                    IdCaSi = caSi.IdCaSi
                };
                _caSiHatBaiHatRepository.Add(chVM);
            }       

            return _unitOfWork.Commit();
        }

        public bool Update(BaiHatVM baiHatVM)
        {
            var baiHat = new BaiHat()
            {
                IdBaiHat = baiHatVM.IdBaiHat,
                TenBaiHat = baiHatVM.TenBaiHat,
                LoiBaiHat = baiHatVM.LoiBaiHat,
                NgayCapNhat = baiHatVM.NgayCapNhat.Value,
                SoLuotThich = baiHatVM.SoLuotThich,
                ThoiLuong = baiHatVM.ThoiLuong,
                FileAnh = baiHatVM.FileAnh,
                FileBaiHat = baiHatVM.FileBaiHat,
                IdTheLoai = Guid.Parse(baiHatVM.IdTheLoai)
            };

            if (!String.IsNullOrEmpty(baiHatVM.IdAlbum))
            {
                baiHat.IdAlbum = Guid.Parse(baiHatVM.IdAlbum);
            }

            _baiHatRepository.Update(baiHat);

            var caSiList = _caSiHatBaiHatRepository.GetByBaiHat(baiHatVM.IdBaiHat);
            foreach (var caSi in caSiList)
            {
                _caSiHatBaiHatRepository.Delete(caSi);
            }

            foreach (var caSi in baiHatVM.CaSi)
            {
                var chVM = new CaSiHatBaiHat()
                {
                    Id = Guid.NewGuid(),
                    IdBaiHat = baiHatVM.IdBaiHat,
                    IdCaSi = caSi.IdCaSi
                };
                _caSiHatBaiHatRepository.Add(chVM);
            }

            return _unitOfWork.Commit();
        }

        public bool Delete(Guid id)
        {
            _baiHatRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public BaiHatVM Detail(Guid id)
        {
            var baiHat = _baiHatRepository.GetById(id);
            if (baiHat == null) return null;
            var caSiList = _caSiHatBaiHatRepository.GetByBaiHat(baiHat.IdBaiHat);
            var caSiVMList = new List<CaSiHatBaiHatVM>();
            foreach (var caSi in caSiList)
            {
                var caSiVM = new CaSiHatBaiHatVM()
                {
                    IdCaSi = caSi.IdCaSi,
                    TenCaSi = caSi.CaSi.TenCaSi
                };
                caSiVMList.Add(caSiVM);
            }
            var baiHatVM = new BaiHatVM()
            {
                IdBaiHat = baiHat.IdBaiHat,
                TenBaiHat = baiHat.TenBaiHat,
                LoiBaiHat = baiHat.LoiBaiHat,
                NgayCapNhat = baiHat.NgayCapNhat,
                SoLuotThich = baiHat.SoLuotThich,
                ThoiLuong = baiHat.ThoiLuong,
                IdAlbum = baiHat.IdAlbum.ToString(),
                IdTheLoai = baiHat.IdTheLoai.ToString(),
                FileAnh = baiHat.FileAnh,
                FileBaiHat = baiHat.FileBaiHat,
                CaSi = caSiVMList
            };

            return baiHatVM;
        }

        public List<BaiHatVM> GetAll()
        {
            var baiHatList = _baiHatRepository.GetAll().OrderBy(x=>x.TenBaiHat);
            var baiHatVMList = new List<BaiHatVM>();
            foreach (var baiHat in baiHatList)
            {
                var caSiList = _caSiHatBaiHatRepository.GetByBaiHat(baiHat.IdBaiHat);
                var caSiVMList = new List<CaSiHatBaiHatVM>();
                foreach (var caSi in caSiList)
                {
                    var caSiVM = new CaSiHatBaiHatVM()
                    {
                        IdCaSi = caSi.IdCaSi,
                        TenCaSi = caSi.CaSi.TenCaSi
                    };
                    caSiVMList.Add(caSiVM);
                }
                
                baiHatVMList.Add(new BaiHatVM()
                {
                    IdBaiHat = baiHat.IdBaiHat,
                    LoiBaiHat = baiHat.LoiBaiHat,
                    NgayCapNhat = baiHat.NgayCapNhat,
                    SoLuotThich = baiHat.SoLuotThich,
                    TenBaiHat = baiHat.TenBaiHat,
                    ThoiLuong = baiHat.ThoiLuong,
                    IdAlbum = baiHat.IdAlbum.ToString(),
                    IdTheLoai = baiHat.IdTheLoai.ToString(),
                    FileAnh = baiHat.FileAnh,
                    FileBaiHat = baiHat.FileBaiHat,
                    CaSi = caSiVMList
                });
            }
            return baiHatVMList;
        }   

        public List<BaiHatVM> GetBaiHatTheoAlbum(string idAlbum)
        {
            var baiHatList = _baiHatRepository.GetBaiHatTheoAlbum(idAlbum);
            var baiHatVMList = new List<BaiHatVM>();
            foreach (var baiHat in baiHatList)
            {
                var caSiList = _caSiHatBaiHatRepository.GetByBaiHat(baiHat.IdBaiHat);
                var caSiVMList = new List<CaSiHatBaiHatVM>();
                foreach (var caSi in caSiList)
                {
                    var caSiVM = new CaSiHatBaiHatVM()
                    {
                        IdCaSi = caSi.IdCaSi,
                        TenCaSi = caSi.CaSi.TenCaSi
                    };
                    caSiVMList.Add(caSiVM);
                }

                baiHatVMList.Add(new BaiHatVM()
                {
                    IdBaiHat = baiHat.IdBaiHat,
                    LoiBaiHat = baiHat.LoiBaiHat,
                    NgayCapNhat = baiHat.NgayCapNhat,
                    SoLuotThich = baiHat.SoLuotThich,
                    TenBaiHat = baiHat.TenBaiHat,
                    ThoiLuong = baiHat.ThoiLuong,
                    IdAlbum = baiHat.IdAlbum.ToString(),
                    IdTheLoai = baiHat.IdTheLoai.ToString(),
                    FileAnh = baiHat.FileAnh,
                    FileBaiHat = baiHat.FileBaiHat,
                    CaSi = caSiVMList
                });
            }
            return baiHatVMList;
        }

        public List<BaiHatVM> GetBaiHatTheoCaSi(string idCaSi)
        {
            var baiHatList = _baiHatRepository.GetBaiHatTheoCaSi(idCaSi);
            var baiHatVMList = new List<BaiHatVM>();
            foreach (var baiHat in baiHatList)
            {
                var caSiList = _caSiHatBaiHatRepository.GetByBaiHat(baiHat.IdBaiHat);
                var caSiVMList = new List<CaSiHatBaiHatVM>();
                foreach (var caSi in caSiList)
                {
                    var caSiVM = new CaSiHatBaiHatVM()
                    {
                        IdCaSi = caSi.IdCaSi,
                        TenCaSi = caSi.CaSi.TenCaSi
                    };
                    caSiVMList.Add(caSiVM);
                }

                baiHatVMList.Add(new BaiHatVM()
                {
                    IdBaiHat = baiHat.IdBaiHat,
                    LoiBaiHat = baiHat.LoiBaiHat,
                    NgayCapNhat = baiHat.NgayCapNhat,
                    SoLuotThich = baiHat.SoLuotThich,
                    TenBaiHat = baiHat.TenBaiHat,
                    ThoiLuong = baiHat.ThoiLuong,
                    IdAlbum = baiHat.IdAlbum.ToString(),
                    IdTheLoai = baiHat.IdTheLoai.ToString(),
                    FileAnh = baiHat.FileAnh,
                    FileBaiHat = baiHat.FileBaiHat,
                    CaSi = caSiVMList
                });
            }
            return baiHatVMList;
        }

        public List<BaiHatVM> GetBaiHatTheoTheLoai(string idTheLoai)
        {
            var baiHatList = _baiHatRepository.GetBaiHatTheoTheLoai(idTheLoai);
            var baiHatVMList = new List<BaiHatVM>();
            foreach (var baiHat in baiHatList)
            {
                var caSiList = _caSiHatBaiHatRepository.GetByBaiHat(baiHat.IdBaiHat);
                var caSiVMList = new List<CaSiHatBaiHatVM>();
                foreach (var caSi in caSiList)
                {
                    var caSiVM = new CaSiHatBaiHatVM()
                    {
                        IdCaSi = caSi.IdCaSi,
                        TenCaSi = caSi.CaSi.TenCaSi
                    };
                    caSiVMList.Add(caSiVM);
                }

                baiHatVMList.Add(new BaiHatVM()
                {
                    IdBaiHat = baiHat.IdBaiHat,
                    LoiBaiHat = baiHat.LoiBaiHat,
                    NgayCapNhat = baiHat.NgayCapNhat,
                    SoLuotThich = baiHat.SoLuotThich,
                    TenBaiHat = baiHat.TenBaiHat,
                    ThoiLuong = baiHat.ThoiLuong,
                    IdAlbum = baiHat.IdAlbum.ToString(),
                    IdTheLoai = baiHat.IdTheLoai.ToString(),
                    FileAnh = baiHat.FileAnh,
                    FileBaiHat = baiHat.FileBaiHat,
                    CaSi = caSiVMList
                });
            }
            return baiHatVMList;
        }

        public List<BaiHatVM> SearchBaiHat(string querry)
        {
            var baiHatList = _baiHatRepository.SearchBaiHat(querry);
            var baiHatVMList = new List<BaiHatVM>();
            foreach (var baiHat in baiHatList)
            {
                var caSiList = _caSiHatBaiHatRepository.GetByBaiHat(baiHat.IdBaiHat);
                var caSiVMList = new List<CaSiHatBaiHatVM>();
                foreach (var caSi in caSiList)
                {
                    var caSiVM = new CaSiHatBaiHatVM()
                    {
                        IdCaSi = caSi.IdCaSi,
                        TenCaSi = caSi.CaSi.TenCaSi
                    };
                    caSiVMList.Add(caSiVM);
                }

                baiHatVMList.Add(new BaiHatVM()
                {
                    IdBaiHat = baiHat.IdBaiHat,
                    LoiBaiHat = baiHat.LoiBaiHat,
                    NgayCapNhat = baiHat.NgayCapNhat,
                    SoLuotThich = baiHat.SoLuotThich,
                    TenBaiHat = baiHat.TenBaiHat,
                    ThoiLuong = baiHat.ThoiLuong,
                    IdAlbum = baiHat.IdAlbum.ToString(),
                    IdTheLoai = baiHat.IdTheLoai.ToString(),
                    FileAnh = baiHat.FileAnh,
                    FileBaiHat = baiHat.FileBaiHat,
                    CaSi = caSiVMList
                });
            }
            return baiHatVMList;
        }

        public List<BaiHatVM> NewestBaiHat(int number)
        {
            var baiHatList = _baiHatRepository.NewestBaiHat(number);
            var baiHatVMList = new List<BaiHatVM>();
            foreach (var baiHat in baiHatList)
            {
                var caSiList = _caSiHatBaiHatRepository.GetByBaiHat(baiHat.IdBaiHat);
                var caSiVMList = new List<CaSiHatBaiHatVM>();
                foreach (var caSi in caSiList)
                {
                    var caSiDetail = _caSiRepository.GetById(caSi.IdCaSi);
                    var caSiVM = new CaSiHatBaiHatVM()
                    {
                        IdCaSi = caSi.IdCaSi,
                        TenCaSi = caSi.CaSi.TenCaSi
                    };
                    caSiVMList.Add(caSiVM);
                }

                baiHatVMList.Add(new BaiHatVM()
                {
                    IdBaiHat = baiHat.IdBaiHat,
                    LoiBaiHat = baiHat.LoiBaiHat,
                    NgayCapNhat = baiHat.NgayCapNhat,
                    SoLuotThich = baiHat.SoLuotThich,
                    TenBaiHat = baiHat.TenBaiHat,
                    ThoiLuong = baiHat.ThoiLuong,
                    IdAlbum = baiHat.IdAlbum.ToString(),
                    IdTheLoai = baiHat.IdTheLoai.ToString(),
                    FileAnh = baiHat.FileAnh,
                    FileBaiHat = baiHat.FileBaiHat,
                    CaSi = caSiVMList
                });
            }
            return baiHatVMList;
        }

    }
}