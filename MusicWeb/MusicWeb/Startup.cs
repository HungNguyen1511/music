using Data.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Service.Business;
using Service.Business.Interfaces;
using Service.Repositorys;
using Service.Repositorys.IRepository;
using Service.UnitOfWork;
using System.IO;

namespace MusicWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<MusicDBContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddCors(o => o.AddPolicy("netcoreMusicPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<ITheLoaiRepository, TheLoaiRepository>();
            services.AddTransient<ITheLoaiBusiness, TheLoaiBusiness>();

            services.AddTransient<ICaSiRepository, CaSiRepository>();
            services.AddTransient<ICaSiBusiness, CaSiBusiness>();

            services.AddTransient<ICaSiHatBaiHatRepository, CaSiHatBaiHatRepository>();
            services.AddTransient<ICaSiHatBaiHatBusiness, CaSiHatBaiHatBusiness>();

            services.AddTransient<IBaiHatRepository, BaiHatRepository>();
            services.AddTransient<IBaiHatBusiness, BaiHatBusiness>();

            services.AddTransient<IAlbumRepository, AlbumRepository>();
            services.AddTransient<IAlbumBusiness, AlbumBusiness>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("netcoreMusicPolicy");
            app.UseHttpsRedirection();
            app.UseStaticFiles();     

            app.UseRouting();

            app.UseAuthorization();
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
                            Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot/Uploads")),
                RequestPath = new PathString("/Uploads")
            });


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}