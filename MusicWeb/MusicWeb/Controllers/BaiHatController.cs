﻿using MediaToolkit;
using MediaToolkit.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting.Internal;
using Service.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using ViewModel;

namespace MusicWeb.Controllers
{
    [Route("api/baihat")]
    [ApiController]
    public class BaiHatController : ControllerBase
    {
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly IBaiHatBusiness baiHatBusiness;
        private readonly ICaSiBusiness caSiBusiness;
        public BaiHatController(IWebHostEnvironment environment, IBaiHatBusiness _baiHatBusiness, ICaSiBusiness _caSiBusiness)
        {
            webHostEnvironment = environment;
            baiHatBusiness = _baiHatBusiness;
            caSiBusiness = _caSiBusiness;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var list = baiHatBusiness.GetAll();
            return Ok(list);
        }

        [HttpGet("{id}", Name = "baihat.get")]
        public IActionResult GetById(string id)
        {
            Guid guid = Guid.Parse(id);
            var baiHat = baiHatBusiness.Detail(guid);
            if (baiHat == null)
            {
                return NotFound();
            }
            return Ok(baiHat);
        }

        [HttpPost]
        public IActionResult Create([FromForm] BaiHatUploadVM baiHatUploadVM)
        {
            if (baiHatUploadVM == null || !ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            baiHatUploadVM.IdBaiHat = Guid.NewGuid();
            baiHatUploadVM.NgayCapNhat = DateTime.Now;

            if (baiHatUploadVM.FormFileBaiHat != null)
            {
                baiHatUploadVM.FileBaiHat = UploadFile(baiHatUploadVM.FormFileBaiHat, baiHatUploadVM.IdBaiHat, "audio");

                var folderPath = Path.Combine(webHostEnvironment.WebRootPath, @"Uploads\Musics\" + baiHatUploadVM.IdBaiHat);
                baiHatUploadVM.ThoiLuong = GetDuration(folderPath + @"\" + baiHatUploadVM.FileBaiHat);
            }

            if (baiHatUploadVM.FormFileAnh != null)
            {
                baiHatUploadVM.FileAnh = UploadFile(baiHatUploadVM.FormFileAnh, baiHatUploadVM.IdBaiHat, "image");
            }

            var caSiVMList = new List<CaSiHatBaiHatVM>();
            foreach (var idCaSi in baiHatUploadVM.CaSi.Split(","))
            {
                var caSi = caSiBusiness.Detail(Guid.Parse(idCaSi));
                var caSiVM = new CaSiHatBaiHatVM()
                {
                    IdCaSi = caSi.IdCaSi,
                    TenCaSi = caSi.TenCaSi
                };
                caSiVMList.Add(caSiVM);
            }

            var baiHatVM = new BaiHatVM()
            {
                IdBaiHat = baiHatUploadVM.IdBaiHat,
                TenBaiHat = baiHatUploadVM.TenBaiHat,
                LoiBaiHat = baiHatUploadVM.LoiBaiHat,
                NgayCapNhat = baiHatUploadVM.NgayCapNhat,
                SoLuotThich = baiHatUploadVM.SoLuotThich,
                ThoiLuong = baiHatUploadVM.ThoiLuong,
                IdTheLoai = baiHatUploadVM.IdTheLoai,
                FileAnh = baiHatUploadVM.FileAnh,
                FileBaiHat = baiHatUploadVM.FileBaiHat,
                CaSi = caSiVMList
            };

            if (!String.IsNullOrEmpty(baiHatUploadVM.IdAlbum))
            {
                baiHatVM.IdAlbum = baiHatUploadVM.IdAlbum;
            }

            if (!baiHatBusiness.Add(baiHatVM))
            {
                ModelState.AddModelError("", $"Can not create {baiHatVM.TenBaiHat}");
                return StatusCode(500, ModelState);
            }

            return CreatedAtRoute("baihat.get", new { id = baiHatVM.IdBaiHat.ToString() }, baiHatVM);
        }     

        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromForm] BaiHatUploadVM baiHatUploadVM)
        {
            var oldBaiHatVM = baiHatBusiness.Detail(baiHatUploadVM.IdBaiHat);
            if (id != baiHatUploadVM.IdBaiHat.ToString() || baiHatUploadVM == null || !ModelState.IsValid || oldBaiHatVM == null)
            {
                return BadRequest(ModelState);
            }

            baiHatUploadVM.NgayCapNhat = oldBaiHatVM.NgayCapNhat;
            if (baiHatUploadVM.FormFileBaiHat != null)
            {
                baiHatUploadVM.FileBaiHat = UploadFile(baiHatUploadVM.FormFileBaiHat, baiHatUploadVM.IdBaiHat, "audio");
                
                var folderPath = Path.Combine(webHostEnvironment.WebRootPath, @"Uploads\Musics\" + baiHatUploadVM.IdBaiHat);
                baiHatUploadVM.ThoiLuong = GetDuration(folderPath + @"\" + baiHatUploadVM.FileBaiHat);
            }
            else
            {
                baiHatUploadVM.FileBaiHat = oldBaiHatVM.FileBaiHat;
                baiHatUploadVM.ThoiLuong = oldBaiHatVM.ThoiLuong;
            }

            if (baiHatUploadVM.FormFileAnh != null)
            {
                baiHatUploadVM.FileAnh = UploadFile(baiHatUploadVM.FormFileAnh, baiHatUploadVM.IdBaiHat, "image");
            }
            else
            {
                baiHatUploadVM.FileAnh = oldBaiHatVM.FileAnh;
            }

            var caSiVMList = new List<CaSiHatBaiHatVM>();
            foreach (var idCaSi in baiHatUploadVM.CaSi.Split(","))
            {
                var caSi = caSiBusiness.Detail(Guid.Parse(idCaSi));
                var caSiVM = new CaSiHatBaiHatVM()
                {
                    IdCaSi = caSi.IdCaSi,
                    TenCaSi = caSi.TenCaSi
                };
                caSiVMList.Add(caSiVM);
            }

            var baiHatVM = new BaiHatVM()
            {
                IdBaiHat = baiHatUploadVM.IdBaiHat,
                TenBaiHat = baiHatUploadVM.TenBaiHat,
                LoiBaiHat = baiHatUploadVM.LoiBaiHat,
                NgayCapNhat = baiHatUploadVM.NgayCapNhat,
                SoLuotThich = baiHatUploadVM.SoLuotThich,
                ThoiLuong = baiHatUploadVM.ThoiLuong,
                IdAlbum = baiHatUploadVM.IdAlbum,
                IdTheLoai = baiHatUploadVM.IdTheLoai,
                FileAnh = baiHatUploadVM.FileAnh,
                FileBaiHat = baiHatUploadVM.FileBaiHat,
                CaSi = caSiVMList
            };

            if (!String.IsNullOrEmpty(baiHatUploadVM.IdAlbum))
            {
                baiHatVM.IdAlbum = baiHatUploadVM.IdAlbum;
            }

            if (!baiHatBusiness.Update(baiHatVM))
            {
                ModelState.AddModelError("", $"Can not update {baiHatVM.TenBaiHat}");
                return StatusCode(500, ModelState);
            }
            return CreatedAtRoute("baihat.get", new { id = baiHatVM.IdBaiHat.ToString() }, baiHatVM);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            Guid guid = Guid.Parse(id);
            if (baiHatBusiness.Detail(guid) == null)
            {
                return NotFound();
            }
            if (!baiHatBusiness.Delete(guid))
            {
                ModelState.AddModelError("", "Can not delete!");
                return StatusCode(500, ModelState);
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();

            var folderPath = Path.Combine(webHostEnvironment.WebRootPath, @"Uploads\Musics\" + id);
            if (Directory.Exists(folderPath))
            {
                Directory.Delete(folderPath, true);
            }
            return NoContent();
        }

        [HttpGet("byAlbum/{id}")]
        public IActionResult GetByAlbum(string id)
        {
            var list = baiHatBusiness.GetBaiHatTheoAlbum(id);
            return Ok(list);
        }

        [HttpGet("byGenre/{id}")]
        public IActionResult GetByTheLoai(string id)
        {
            var list = baiHatBusiness.GetBaiHatTheoTheLoai(id);
            return Ok(list);
        }

        [HttpGet("byArtist/{id}")]
        public IActionResult GetByCaSi(string id)
        {
            var list = baiHatBusiness.GetBaiHatTheoCaSi(id);
            return Ok(list);
        }

        [HttpGet("search/{query}")]
        public IActionResult Search(string query)
        {
            var list = baiHatBusiness.SearchBaiHat(query);
            return Ok(list);
        }

        [HttpGet("newest/{number:int?}")]
        public IActionResult GetNewest(int? number = 10)
        {
            var list = baiHatBusiness.NewestBaiHat(number.Value);
            return Ok(list);
        }

        [HttpPost("like/{id}")]
        public IActionResult Like(string id)
        {
            Guid guid = Guid.Parse(id);
            var music = baiHatBusiness.Detail(guid);
            if (music == null) return NotFound();
            music.SoLuotThich += 1;
            if (!baiHatBusiness.Update(music))
            {
                ModelState.AddModelError("", $"Can not update {music.TenBaiHat}");
                return StatusCode(500, ModelState);
            }
            return StatusCode(StatusCodes.Status204NoContent);
        }

        private string GetUniqueFileName(string fileName, string id, string type)
        {
            fileName = Path.GetFileName(fileName);
            return type.ToLower()
                      + "_"
                      + id
                      + Path.GetExtension(fileName);
        }

        private string UploadFile(IFormFile formFile, Guid idBaiHat, string type)
        {
            Guid guid = Guid.NewGuid();
            string id = guid.ToString().Substring(0, 4);
            var uniqueFileName = GetUniqueFileName(formFile.FileName, id, type);
            var uploads = Path.Combine(webHostEnvironment.WebRootPath, @"Uploads\Musics\" + idBaiHat);
            var filePath = Path.Combine(uploads, uniqueFileName);

            bool uploadPath = Directory.Exists(uploads);
            if (!uploadPath)
                Directory.CreateDirectory(uploads);

            formFile.CopyTo(new FileStream(filePath, FileMode.Create));
            return uniqueFileName;
        }

        private int GetDuration(string fileName)
        {
            var inputFile = new MediaFile { Filename = fileName };

            using (var engine = new Engine())
            {
                engine.GetMetadata(inputFile);
            }
            return Convert.ToInt32(inputFile.Metadata.Duration.TotalSeconds);
        }
    }
}