﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.Business.Interfaces;
using System;
using System.IO;
using System.Text.RegularExpressions;
using ViewModel;

namespace MusicWeb.Controllers
{
    [Route("api/casi")]
    [ApiController]
    public class CaSiController : ControllerBase
    {
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly ICaSiBusiness caSiBusiness;
        private readonly ICaSiHatBaiHatBusiness caSiHatBaiHatBusiness;

        public CaSiController(IWebHostEnvironment environment, ICaSiBusiness _caSiBusiness, ICaSiHatBaiHatBusiness _caSiHatBaiHatBusiness)
        {
            webHostEnvironment = environment;
            caSiBusiness = _caSiBusiness;
            caSiHatBaiHatBusiness = _caSiHatBaiHatBusiness;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var list = caSiBusiness.GetAll();
            return Ok(list);
        }

        [HttpGet("search/{query}")]
        public IActionResult Search(string query)
        {
            var list = caSiBusiness.SearchCaSi(query);
            return Ok(list);
        }


        [HttpGet("{id}", Name = "casi.get")]
        public IActionResult GetById(string id)
        {
            Guid guid = Guid.Parse(id);
            var caSi = caSiBusiness.Detail(guid);
            if (caSi == null)
            {
                return NotFound();
            }
            return Ok(caSi);
        }


        [HttpGet("byMusic/{id}")]
        public IActionResult GetByMusic(string id)
        {
            Guid guid = Guid.Parse(id);
            var caSi = caSiBusiness.GetByMusic(guid);
            if (caSi == null)
            {
                return NotFound();
            }
            return Ok(caSi);
        }

        [HttpPost]
        public IActionResult Create([FromForm] CaSiUploadVM caSiUploadVM)
        {
            if (caSiUploadVM == null || !ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            caSiUploadVM.IdCaSi = Guid.NewGuid();

            if (caSiUploadVM.FormFileAnh != null)
            {
                caSiUploadVM.AnhCaSi = UploadFile(caSiUploadVM.FormFileAnh, caSiUploadVM.IdCaSi, "artist");
            }

            var caSiVM = new CaSiVM()
            {
                IdCaSi = caSiUploadVM.IdCaSi,
                TenCaSi = caSiUploadVM.TenCaSi,
                TieuSu = caSiUploadVM.TieuSu,
                AnhCaSi = caSiUploadVM.AnhCaSi
            };

            if (!caSiBusiness.Add(caSiVM))
            {
                ModelState.AddModelError("", $"Can not create {caSiVM.TenCaSi}");
                return StatusCode(500, ModelState);
            }
            return CreatedAtRoute("casi.get", new { id = caSiVM.IdCaSi.ToString() }, caSiVM);
        }

        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromForm] CaSiUploadVM caSiUploadVM)
        {
            var oldCaSi = caSiBusiness.Detail(caSiUploadVM.IdCaSi);
            if (caSiUploadVM == null || id != caSiUploadVM.IdCaSi.ToString() || oldCaSi == null)
            {
                return BadRequest(ModelState);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (caSiUploadVM.FormFileAnh != null)
            {
                caSiUploadVM.AnhCaSi = UploadFile(caSiUploadVM.FormFileAnh, caSiUploadVM.IdCaSi, "artist");
            }
            else
            {
                caSiUploadVM.AnhCaSi = oldCaSi.AnhCaSi;
            }

            var caSiVM = new CaSiVM()
            {
                IdCaSi = caSiUploadVM.IdCaSi,
                TenCaSi = caSiUploadVM.TenCaSi,
                TieuSu = caSiUploadVM.TieuSu,
                AnhCaSi = caSiUploadVM.AnhCaSi
            };

            if (!caSiBusiness.Update(caSiVM))
            {
                ModelState.AddModelError("", $"Can not update {caSiVM.TenCaSi}");
                return StatusCode(500, ModelState);
            }
            return CreatedAtRoute("casi.get", new { id = caSiVM.IdCaSi.ToString() }, caSiVM);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            Guid guid = Guid.Parse(id);
            if (caSiBusiness.Detail(guid) == null)
            {
                return NotFound();
            }
            if (!caSiBusiness.Delete(guid))
            {
                ModelState.AddModelError("", "Can not delete!");
                return StatusCode(500, ModelState);
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
            
            var folderPath = Path.Combine(webHostEnvironment.WebRootPath, @"Uploads\Artists\" + id);
            if (Directory.Exists(folderPath))
            {
                Directory.Delete(folderPath, true);
            }
            return NoContent();
        }

        private string GetUniqueFileName(string fileName, string id, string type)
        {
            fileName = Path.GetFileName(fileName);
            return type.ToLower()
                      + "_"
                      + id
                      + Path.GetExtension(fileName);
        }

        private string UploadFile(IFormFile formFile, Guid idCaSi, string type)
        {
            Guid guid = Guid.NewGuid();
            string id = guid.ToString().Substring(0,4);
            var uniqueFileName = GetUniqueFileName(formFile.FileName, id, type);
            var uploads = Path.Combine(webHostEnvironment.WebRootPath, @"Uploads\Artists\" + idCaSi);
            var filePath = Path.Combine(uploads, uniqueFileName);

            bool uploadPath = Directory.Exists(uploads);
            if (!uploadPath)
                Directory.CreateDirectory(uploads);

            formFile.CopyTo(new FileStream(filePath, FileMode.Create));
            return uniqueFileName;
        }
    }
}