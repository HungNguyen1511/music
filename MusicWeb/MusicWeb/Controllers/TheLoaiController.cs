﻿using Microsoft.AspNetCore.Mvc;
using Service.Business.Interfaces;
using System;
using ViewModel;

namespace MusicWeb.Controllers
{
    [Route("api/theloai")]
    [ApiController]
    public class TheLoaiController : ControllerBase
    {
        private readonly ITheLoaiBusiness theLoaiBusiness;

        public TheLoaiController(ITheLoaiBusiness _theLoaiBusiness)
        {
            theLoaiBusiness = _theLoaiBusiness;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var list = theLoaiBusiness.GetAll();
            return Ok(list);
        }

        [HttpGet("{id}", Name = "theloai.get")]
        public IActionResult GetById(string id)
        {
            Guid guid = Guid.Parse(id);
            var theLoai = theLoaiBusiness.Detail(guid);
            if(theLoai == null)
            {
                return NotFound();
            }
            return Ok(theLoai);
        }

        [HttpPost]
        public IActionResult Create([FromBody] TheLoaiVM theLoaiVM)
        {
            if(theLoaiVM == null || !ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            theLoaiVM.IdTheLoai = Guid.NewGuid();
            if (!theLoaiBusiness.Add(theLoaiVM))
            {
                ModelState.AddModelError("", $"Something went wrong when creating the record {theLoaiVM.TenTheLoai}");
                return StatusCode(500, ModelState);
            }       
            return CreatedAtRoute("theloai.get", new { id = theLoaiVM.IdTheLoai.ToString() }, theLoaiVM);
        }

        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromBody] TheLoaiVM theLoaiVM)
        {
            if (id != theLoaiVM.IdTheLoai.ToString() || theLoaiVM == null || !ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (!theLoaiBusiness.Update(theLoaiVM))
            {
                ModelState.AddModelError("", $"Something went wrong when updating the record {theLoaiVM.TenTheLoai}");
                return StatusCode(500, ModelState);
            }
            return CreatedAtRoute("theloai.get", new { id = theLoaiVM.IdTheLoai.ToString() }, theLoaiVM);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            Guid guid = Guid.Parse(id);
            if (theLoaiBusiness.Detail(guid) == null)
            {
                return NotFound();
            }
            if (!theLoaiBusiness.Delete(guid))
            {
                ModelState.AddModelError("", "Something went wrong when deleting the record");
                return StatusCode(500, ModelState);
            }
            return NoContent();
        }
    }
}