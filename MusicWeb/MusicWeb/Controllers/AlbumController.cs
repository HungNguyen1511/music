﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using ViewModel;

namespace MusicWeb.Controllers
{
    [Route("api/album")]
    [ApiController]
    public class AlbumController : ControllerBase
    {
        private readonly IAlbumBusiness albumBusiness;
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly ICaSiBusiness caSiBusiness;
        private readonly IBaiHatBusiness baiHatBusiness;

        public AlbumController(IWebHostEnvironment environment, IAlbumBusiness _albumBusiness, ICaSiBusiness _caSiBusiness, IBaiHatBusiness _baiHatBusiness)
        {
            webHostEnvironment = environment;
            albumBusiness = _albumBusiness;
            caSiBusiness = _caSiBusiness;
            baiHatBusiness = _baiHatBusiness;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var list = albumBusiness.GetAll();
            return Ok(list);
        }

        [HttpGet("{id}", Name = "album.get")]
        public IActionResult GetById(string id)
        {
            Guid guid = Guid.Parse(id);
            var album = albumBusiness.Detail(guid);
            if (album == null)
            {
                return NotFound();
            }
            return Ok(album);
        }

        [HttpPost]
        public IActionResult Create([FromForm] AlbumUploadVM albumUploadVM)
        {
            if (albumUploadVM == null || !ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            albumUploadVM.IdAlbum = Guid.NewGuid();
            albumUploadVM.NgayPhatHanh = DateTime.Now;

            if (albumUploadVM.FormFileAnh != null)
            {
                albumUploadVM.AnhAlbum = UploadFile(albumUploadVM.FormFileAnh, albumUploadVM.IdAlbum, "Album");
            }

            var caSi = caSiBusiness.Detail(albumUploadVM.IdCaSi);
            var baiHats = new List<Guid>();
            if (albumUploadVM.BaiHat != null)
            {
                foreach (var item in albumUploadVM.BaiHat.Split(","))
                {
                    baiHats.Add(Guid.Parse(item));
                }
            }

            var albumVM = new AlbumVM()
            {
                IdAlbum = albumUploadVM.IdAlbum,
                TenAlbum = albumUploadVM.TenAlbum,
                AnhAlbum = albumUploadVM.AnhAlbum,
                NgayPhatHanh = albumUploadVM.NgayPhatHanh,
                CaSi = caSi,
                BaiHats = baiHats
            };

            if (!albumBusiness.Add(albumVM))
            {
                ModelState.AddModelError("", $"Something went wrong when creating the record {albumVM.TenAlbum}");
                return StatusCode(500, ModelState);
            }
           
            return CreatedAtRoute("album.get", new { id = albumVM.IdAlbum.ToString() }, albumVM);
        }

        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromForm] AlbumUploadVM albumUploadVM)
        {
            Guid guid = Guid.Parse(id);
            var oldAlbum = albumBusiness.Detail(guid);
            if (albumUploadVM == null || !ModelState.IsValid || oldAlbum == null || guid != albumUploadVM.IdAlbum)
            {
                return BadRequest(ModelState);
            }

            albumUploadVM.NgayPhatHanh = oldAlbum.NgayPhatHanh;

            if (albumUploadVM.FormFileAnh != null)
            {
                albumUploadVM.AnhAlbum = UploadFile(albumUploadVM.FormFileAnh, albumUploadVM.IdAlbum, "album");
            }
            else
            {
                albumUploadVM.AnhAlbum = oldAlbum.AnhAlbum;
            }

            var caSi = caSiBusiness.Detail(albumUploadVM.IdCaSi);
            var baiHats = new List<Guid>();
            if (albumUploadVM.BaiHat != null)
            {
                foreach (var item in albumUploadVM.BaiHat.Split(","))
                {
                    baiHats.Add(Guid.Parse(item));
                }
            }
            var albumVM = new AlbumVM()
            {
                IdAlbum = albumUploadVM.IdAlbum,
                TenAlbum = albumUploadVM.TenAlbum,
                AnhAlbum = albumUploadVM.AnhAlbum,
                NgayPhatHanh = albumUploadVM.NgayPhatHanh,
                CaSi = caSi,
                BaiHats = baiHats
            };

            if (!albumBusiness.Update(albumVM))
            {
                ModelState.AddModelError("", $"Something went wrong when updating the record {albumVM.TenAlbum}");
                return StatusCode(500, ModelState);
            }

            return CreatedAtRoute("album.get", new { id = albumVM.IdAlbum.ToString() }, albumVM);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            Guid guid = Guid.Parse(id);
            if (albumBusiness.Detail(guid) == null)
            {
                return NotFound();
            }
            if (!albumBusiness.Delete(guid))
            {
                ModelState.AddModelError("", "Something went wrong when deleting the record");
                return StatusCode(500, ModelState);
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();

            var folderPath = Path.Combine(webHostEnvironment.WebRootPath, @"Uploads\Albums\" + id);
            if (Directory.Exists(folderPath))
            {
                Directory.Delete(folderPath, true);
            }
            return NoContent();
        }

        [HttpGet("newest/{number:int}")]
        public IActionResult GetNewestAlbum(int number)
        {
            var list = albumBusiness.GetNewestAlbum(number);
            return Ok(list);
        }

        [HttpGet("byArtist/{artistId}")]
        public IActionResult GetAlbumArtist(string artistId)
        {
            var list = albumBusiness.GetAlbumCasi(artistId);
            return Ok(list);
        }

        [HttpGet("search/{query}")]
        public IActionResult GetNewestAlbum(string query)
        {
            var list = albumBusiness.SearchAlbum(query);
            return Ok(list);
        }

        private string GetUniqueFileName(string fileName, string id, string type)
        {
            fileName = Path.GetFileName(fileName);
            return type.ToLower()
                      + "_"
                      + id
                      + Path.GetExtension(fileName);
        }

        private string UploadFile(IFormFile formFile, Guid idAlbum, string type)
        {
            Guid guid = Guid.NewGuid();
            string id = guid.ToString().Substring(0, 4);
            var uniqueFileName = GetUniqueFileName(formFile.FileName, id, type);
            var uploads = Path.Combine(webHostEnvironment.WebRootPath, @"Uploads\Albums\" + idAlbum);
            var filePath = Path.Combine(uploads, uniqueFileName);

            bool uploadPath = Directory.Exists(uploads);
            if (!uploadPath)
                Directory.CreateDirectory(uploads);

            formFile.CopyTo(new FileStream(filePath, FileMode.Create));
            return uniqueFileName;
        }
    }
}